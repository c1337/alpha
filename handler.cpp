#include <random>
#include <iostream>
#include <exception>
#include <algorithm>
//#include <chrono>
#include <limits>
#include "handler.h"

Handler::Handler()
{
}

Handler::~Handler()
{
    print_results();
}

void Handler::print_results()
{

    std::cout << "results:"
              << "\n";
    for (auto& res : results)
    {
        std::cout << res << " ";
    }
    std::cout << "\n";

    std::cout << std::scientific << (float)*std::min_element(results.begin(), results.end()) << "\n";
}

void Handler::run()
{
    // const auto processor_count = std::thread::hardware_concurrency();
    const auto processor_count = 1;
    std::cout << "core count:\t" << processor_count << "\n";

    const uint32_t iterations = 100000000;
    const float global_max = 100000000.0;
    const float global_min = -global_max;

    std::random_device dev;
    std::mt19937 rng(dev());
    workloads::Workload wl;

    std::cout << std::scientific;

    float delta = (global_max - global_min) / processor_count;
    auto t0 = std::chrono::system_clock::now();

    for (int i = 0; i < processor_count; i++)
    {
        const float min = global_min + i * delta;
        const float max = global_min + (i + 1) * delta;
        std::uniform_int_distribution<int> dist6(min, max);
        const float start = dist6(rng);

        threads.emplace_back(std::async(std::launch::async, &workloads::Workload::load, &wl, min, max, iterations, start));
        // threads.emplace_back(std::async(std::launch::async, [wl,min,max,iterations,start](){ wl.load(min,max,iterations,start); } ) ); // TODO: problems
        // using a lambda with non-const method std::cout << "min: " << min << " max: " << max << " start: " << start << "\n";
        // std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    for (auto& asy : threads)
    {
        results_raw.emplace_back(asy.get());
    }
    auto t_end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = t_end - t0;
    std::cout << "run took:\t" << diff.count() << "s"
              << "\n";

    for (auto& e : results_raw)
    {

        if (e.has_value())
        {
            results.emplace_back(e.value());
        }
    }
    std::cout << "\n";
}
