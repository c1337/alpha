
# [project]: alpha

This is the meta project to distribute a workload.

# contents
1. [usage](#1-usage)
2. [setup IDE](#2-setup-ide)
    - [VSCode](#2.1-VSCode)
    - [Eclipse](#2.2-Eclipse)
3. [miscellaneous](#3-miscellanous)

# 1 usage

## 1.1 localhost

```SHELL
git clone --recurse-submodule gitlab.com/c1337/alpha
cd alpha
mkdir build; cd build
#and either
cmake ..
make -j$(nproc)
#or
cmake -G Ninja ..
ninja
# and run
./alpha
```
## 1.2 docker

*coming soon*
- create c++ build container (e.g. [base](https://gitlab.com/homelab202/base))
- setup IDE for remote build

# 2 Setup IDE

## 2.1 VSCode
- install VSCode
- get following Plugins
    - [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)
    - [CMake Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools)
- [how to get started](https://code.visualstudio.com/docs/cpp/cmake-linux)
    - Setup: (Ctrl-Shift+P) to open ">" and enter/choose `CMake: Quick Start`
    - Kit: `> CMake: Select a Kit` and choose any of GCC or Clang, ...
    - Variant: `> CMake: Select Variant` and choose any of Debug, Release, ...
    - Configure: `> CMake: Configure`
    - Build: `> CMake: Build` or press `[F7]`
    - Debug: `> CMake: Debug` or press `Ctrl + [F5]`
    - Run: `> CMake: Run` or press `Shift + [F5]`

## 2.2 Eclipse 

getting frea\*\*\*g eclipse-cpp running under Arch linux / [EndevourOS](http://distrowatch.org/table.php?distribution=endeavour). (contributing a simpler how-to is much appreciated!)

build from source

```
git clone https://aur.archlinux.org/eclipse-cpp.git
cd eclipse-cpp
vim PGKBUILD #check packagebuild
makepkg -csi
```

or by pamac/yay/yaourt...

`yay install eclipse-cpp`

### 2.2 install necessary plugins
Help/Install new Software
switch to work with: `2022-03 - https://download.eclipse.org/releases/2022-03/` or what your date may be, alternatively you can pick all mirrors `--All Available Sites--`, which will take a little longer to load.

under programming lanuages install 
- C/C++ development tools
- C/C++ development tools SDK

With this new projects should receive a *C/C++ Build* Chapter when right click the project in the *Project Explorer* and pick *Properties*.

Furthermore i installed the *Cmake4Eclipse*, as well as the *Remote System Explorer* Plugin.

> Hint: When the first project compiled severeal modules could not be found. -> [right click on your project -> Index -> Rebuild](https://stackoverflow.com/questions/9124882/eclipse-method-could-not-be-resolved-in-a-simple-program-c)

# 3 miscellanous


