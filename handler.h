#pragma once

#include <vector>
#include <thread>
#include <optional>
#include <future>

#include <workload.h>

class Handler
{
public:
    Handler();
    Handler(const Handler&) = delete;
    Handler operator=(const Handler) = delete;

    virtual ~Handler();
    void run();

private:
    void print_results();

    std::vector<std::future<std::optional<float>>> threads;
    std::vector<std::optional<float>> results_raw;
    std::vector<float> results;
};
